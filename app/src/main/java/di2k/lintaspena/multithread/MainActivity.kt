package di2k.lintaspena.multithread

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.*//Created by 디딬 Didik M. Hadiningrat on 22 July 2019
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var url: String = "http://games.farrelstudio.com/"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webView.getSettings().setJavaScriptEnabled(true)

        webView.getSettings().setSupportZoom(true)
        webView.getSettings().setBuiltInZoomControls(true)
        webView.getSettings().setDisplayZoomControls(false)
        webView.loadUrl(url)

        webView.setWebChromeClient(object: WebChromeClient(){
            @Override
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                //progress will be view when reload page
                progressBar.setVisibility(View.VISIBLE)

            }
        })

        webView.setWebViewClient(object: WebViewClient(){
            @Override
            override fun onReceivedError(view: WebView?, errorCode: Int, descriptor: String, failingUrl: String) {
                webView.loadUrl("http://games.farrelstudio.com/")
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                //progressBar will be gone when reload page finished
                super.onPageFinished(view, url)
                progressBar.setVisibility(View.GONE)
                webView.setVisibility(View.VISIBLE)
            }//Created by 디딬 Didik M. Hadiningrat on 22 July 2019
        })


    }

}